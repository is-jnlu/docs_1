# 润和HH-SCDAYU200开发套件

## 简介

- 基于Rockchip RK3568，集成双核心架构GPU以及高效能NPU；
- 板载四核64位Cortex-A55 处理器采用22nm先进工艺，主频高达2.0GHz；
- 支持蓝牙、Wi-Fi、音频、视频和摄像头等功能，拥有丰富的扩展接口，支持多种视频输入输出接口；
- 配置双千兆自适应RJ45以太网口，可满足NVR、工业网关等多网口产品需求。

## 相关教程
[润和DAYU200教程文档](https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/docs/README.md)

波特率：1500000

### 购买指引：

![image.png](https://harmonyos.oss-cn-beijing.aliyuncs.com/images/202204/36430b195b59a7025ad158503ddc466512dcb6.png?x-oss-process=image/resize,w_676,h_320)


润和HH-SCDAYU200开发板外观图如图1所示：

![DAYU200产品图01](picture/HH-SCDAYU200.png)

​                                                                 图1：润和HH-SCDAYU200开发板外观图

## 开发板详情

**1.润和HH-SCDAYU200开发板正面外观图**

![DAYU200接口说明_01]( picture/front.png )

​                                                         图2：润和HH-SCDAYU200开发板正面外观图

**2.润和HH-SCDAYU200开发板反面外观图**

![DAYU200接口说明_02](picture/back.png)

​                                                        图3：润和HH-SCDAYU200开发板反面外观图

**3、接口复用说明**

- BT1120：将 GMAC1 相关引脚用于 BT1120，则能实现 BT1120相关输出（目前底板上没有留BT1120接口）。

- PCIE：目前有 1x 2Lanes PCIe3.0 Connector (RC Mode)，若想增加PCIE2.0，将MULTI_PHY2配置为 PCIE模式。

-  SATA：目前有1x SATA3.0，也可以通过配置 MULTI_PHY0、MULTI_PHY1为SATA,就能实现3x SATA3.0。

- RGMII：目前有2x RGMII，如果想增加QSGMII，需要将MULTI_PHY1、MULTI_PHY2配置为QSGMII。

## 开发板规格

润和HH-SCDAYU200开发板MCU/处理器规格及规格清单如表1所示：

| 配置      | 参数                   |
| ---- | ---- |
| 芯片 | Rockchip RK3568 芯片 |
| 架构 | ARM |
| 主频 | 2.0GHz |
| 工作电压 | 12V/2A |
| 内存&存储 | 2GB LPDRR4 / 32GB EMMC |

​                                                   表1 润和HH-SCDAYU200开发板MCU/处理器规格及规格清单

润和HH-SCDAYU200开发板底板规则说明如表2所示：

| 配置       | 参数                                                         |
| ---------- | ------------------------------------------------------------ |
| 显示接口   | 1x HDMI2.0(Type-A)接口,支持4K/60fps输出 <br />2x MIPI接口，支持1920\*1080@60fps输出 <br />1x eDP接口，支持2K@60fps输出 |
| 音频接口   | 1x 8ch I2S/TDM/PDM  <br />1x HDMI音频输出  <br />1x 喇叭输出  <br />1x 耳机输出  <br />1x 麦克风，板载音频输入 |
| 以太网     | 2x GMAC(10/100/1000M)                                        |
| 无线网络   | SDIO接口，支持WIFI6 5G/2.5G，BT4.2                           |
| 摄像头接口 | MIPI-CSI2, 1x4-lane/2x2-lane@2.5Gbps/lane                    |
| USB        | 2x USB2.0 Host,Type-A  <br />1x USB3.0 Host,Type-A  <br />1x USB3.0 OTG |
| PCIe       | 1x 2Lanes PCIe3.0 Connector (RC Mode)                        |
| SATA       | 1x SATA3.0 Connector                                         |
| SDMMC      | 1x Micro SD Card3.0                                          |
| 按键       | 1x Vol+/Recovery  <br />1x Reset  <br />1x Power  <br />1x Vol-  <br />1x Mute |
| 调试       | 1x 调试串口                                                  |
| RTC        | 1x RTC                                                       |
| IR         | 1x IR                                                        |
| 三色灯     | 3x LED                                                       |
| G-sensor   | 1x G-sensor                                                  |
| FAN        | 1x Fan                                                       |
| 扩展接口   | 20Pin扩展接口包括： <br />2x ADC接口 <br />2x I2C接口 <br />7x GPIO口（或者3x gpio + 4x uart信号） <br />3x VCC电源（12V、3.3V、5V）） |
| 底板尺寸   | 180mm×130mm                                                  |
| PCB 规格   | 4 层板                                                       |

​                                                              表2 润和HH-SCDAYU200开发板底板规则说明

## 开发板功能

- 核心板采用6层布线工艺，尺寸仅82mm×60mm，可满足小型终端产品空间需求


- 高性价比：适合中小规模企业/用户使用


- 双网口：可通过双网口访问和传输内外网数据，提高网络传输效率


- 多屏异显：最多可以满足三屏异显功能


- 支持多系统：支持OpenHarmony、Linux系统

## 应用场景

适用于智能NVR、云终端、物联网网关、工业控制、信息发布终端、多媒体广告机等场景，亦可广泛应用于嵌入式人工智能领域。



## 扩展IO原理图：

![image-20220630204157672](docs/image/image-20220630204157672.png)

## 金手指原理图
![输入图片说明](picture/image_jsz.png)

## 尺寸图

![输入图片说明](picture/image.png)

![输入图片说明](picture/imageLCD.png)

![输入图片说明](picture/imagesxt.png)


## 参考说明

1.[开发环境搭建编译](https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E7%BC%96%E8%AF%91%E6%8C%87%E5%8D%97.md)

2.[摄像头和屏幕接线指导](https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/%E5%BC%80%E5%8F%91%E6%9D%BF%E5%AE%89%E8%A3%85%E6%8E%A5%E7%BA%BF%E6%8C%87%E5%AF%BC.md)

3.[烧录工具和指南](https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97)

