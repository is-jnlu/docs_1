# 润和DAYU200开发套件教程

## 南向开发：
[固件升级](烧录指导文档.md)

[南向开发环境](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-env-setup.md)

[OpenHarmony 3.1 Release docker编译指南](https://ost.51cto.com/posts/11275)

[编写helloworld](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-running-rk3568-create.md)

[三色灯点灯流程](https://ost.51cto.com/posts/11566)

[南向北向的实践--RGB LED 控制器](https://gitee.com/soonliao/dayu-light)


## 应用开发

[eTS之常用控件](https://ost.51cto.com/posts/13184)

[eTS之常用布局](https://ost.51cto.com/posts/13218)

[eTS之网络请求http](https://ost.51cto.com/posts/13219)

[socket通信之UDP](socket通信.md)

[如何在DAYU200上安装浏览器](https://bbs.elecfans.com/jishu_2280142_1_1.html)

## 视频课程

[DAYU200升级OpenHarmony系统](https://www.bilibili.com/video/BV1ar4y1p7U7)

[应用开发入门](https://www.bilibili.com/video/BV1kZ4y1U7wM)
