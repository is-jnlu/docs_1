/*
 * Copyright (c) 2020, HiHope Community.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include "wifiiot_i2c.h"
#include "wifiiot_gpio.h"
#include "wifiiot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "wifiiot_errno.h"
#include "wifiiot_gpio_w800.h"
#include "wifiiot_pwm.h"

static int g_ledStates[3] = {0, 0, 0};
static int g_currentBright = 0;
static int g_beepState = 0;

static void *TrafficLightTask(const char *arg)
{
    (void)arg;

    printf("TrafficLightTask start!\r\n");
    WifiIotGpioIdx pins[] = {WIFI_IOT_GPIO_PB_10, WIFI_IOT_GPIO_PA_01, WIFI_IOT_GPIO_PB_01};
    for (int i = 0; i < 4; i++) {
        for (unsigned int j = 0; j < 3; j++) {
            GpioSetOutputVal(pins[j], WIFI_IOT_GPIO_VALUE1);
            osDelay(200);

            GpioSetOutputVal(pins[j], WIFI_IOT_GPIO_VALUE0);
            osDelay(100);
        }
    }

    while (1) {
        for (unsigned int j = 0; j < 3; j++) {
            GpioSetOutputVal(pins[j], g_ledStates[j]);
        }
        if (g_beepState) {
            PwmStart(WIFI_IOT_PWM_PORT_PWM0, 100, 500);
        } else {
            PwmStop(WIFI_IOT_PWM_PORT_PWM0);
        }
    }

    return NULL;
}

static void OnButtonPressed(char *arg)
{
    (void) arg;
    printf("the button pressed\r\n");
    GpioSetIsrMask(WIFI_IOT_GPIO_PB_05, 0);
    for (int i = 0; i < 3; i++) {
        if (i == g_currentBright) {
            g_ledStates[i] = 1;
        } else {
            g_ledStates[i] = 0;
        }
    }
    g_currentBright++;
    if (g_currentBright == 3) g_currentBright = 0;

    g_beepState = !g_beepState;
}

static void StartTrafficLightTask(void)
{
    osThreadAttr_t attr;

    GpioInit();

    GpioSetDir(WIFI_IOT_GPIO_PB_10, WIFI_IOT_GPIO_DIR_OUTPUT);//red
    GpioSetOutputVal(WIFI_IOT_GPIO_PB_10, WIFI_IOT_GPIO_VALUE0);

    GpioSetDir(WIFI_IOT_GPIO_PA_01, WIFI_IOT_GPIO_DIR_OUTPUT);//green
    GpioSetOutputVal(WIFI_IOT_GPIO_PA_01, WIFI_IOT_GPIO_VALUE0);

    GpioSetDir(WIFI_IOT_GPIO_PB_01, WIFI_IOT_GPIO_DIR_OUTPUT);//yellow
    GpioSetOutputVal(WIFI_IOT_GPIO_PB_01, WIFI_IOT_GPIO_VALUE0);

    GpioSetDir(WIFI_IOT_GPIO_PB_05, WIFI_IOT_GPIO_DIR_INPUT);
    IoSetPull(WIFI_IOT_GPIO_PB_05, WIFI_IOT_GPIO_ATTR_PULLHIGH);
    GpioRegisterIsrFunc(WIFI_IOT_GPIO_PB_05, WIFI_IOT_INT_TYPE_EDGE, WIFI_IOT_GPIO_EDGE_FALL_LEVEL_LOW,
        OnButtonPressed, NULL);

    GpioSetDir(WIFI_IOT_GPIO_PB_00, WIFI_IOT_GPIO_DIR_OUTPUT);
    PwmInit(WIFI_IOT_PWM_PORT_PWM0);

    WatchDogDisable();

    attr.name = "TrafficLightTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024;
    attr.priority = osPriorityNormal;

    if (osThreadNew((osThreadFunc_t)TrafficLightTask, NULL, &attr) == NULL) {
        printf("[LedExample] Falied to create TrafficLightTask!\n");
    }
}

APP_FEATURE_INIT(StartTrafficLightTask);
