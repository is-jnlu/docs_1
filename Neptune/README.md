# Neptune开发板指南



## Neptune模组介绍
![编组 2.png](assets/README.assets/6374675610393593577829134.png) 



高性价比Wi-Fi&蓝牙双模SoC模组。

支持标准802.11 b/g/n协议，内置完整TCP/IP协议栈，集成蓝牙基带处理器，支持BT/BLE4.2 协议采用SMD封装，可通过标准SMT设备实现产品快速生产，为客户提供高可靠性连接方式。

特别适合自动化、大规模、低成本的现代化生产方式，方便应用于各种物联网硬件终端场合。

![Neptune_Hardware_Quick_Reference](assets/README.assets/Neptune_Hardware_Quick_Reference.png)





## 产品参数

| 配置          | 参数                                                         |
| ------------- | ------------------------------------------------------------ |
| SoC           | WinnerMicro W800芯片32位XT804处理器，工作频率240MHz，内置DSP、浮点运算单元与安全引擎 |
| 操作系统      | 支持HarmonyOS、FreeRTOS                                      |
| 存储空间      | 2MB Flash，288KB RAM                                         |
| UART          | 5路UART高速接口                                              |
| ADC           | 2路16比特SD-ADC，最高采样率1KHz                              |
| I2C           | 1个I2C控制器                                             |
| GPIO          | 最多支持 18个GPIO                                            |
| PWM           | 5路PWM接口                                                   |
| I2S           | 1路Duplex I2S控制器                                          |
| Wi-Fi         | 支持GB15629.11-2006，IEEE802.11 b/g/n支持 Wi-Fi WMM/WMM-PS/WPA/WPA2/WPS支持 Station、Soft-AP、Soft-AP/Station 功能 |
| Wi-Fi发射功率 | 802.11b：19±2 dBm (@DSSS 1Mbps) 802.11g：14±2 dBm (@OFDM 54Mbps) 802.11n：12±2 dBm (@OFDM，MCS7，HT20 |
| Wi-Fi接收参数 | CCK，1 Mbps：-93dBmCCK，11 Mbps：-87dBmOFDM，54 Mbps：-73dBmHT20，MCS7：-71dB |
| BT            | 集成蓝牙基带处理器/协议处理器，支持 BT/BLE 双模工作模式，支持 BT/BLE4.2 协议 |
| 蓝牙发射功率  | BLE功率控制范围：-10 ~ 12dBm，典型值6dBmBT功率控制范围：-10 ~ 8dBm，典型值0dB |
| 蓝牙接收参数  | BLE灵敏度：-94 dBm@30.8% PERBT灵敏度：-88 dBm@0.01% BER      |
| 电源管理      | 支持Wi-Fi节能模式功耗管理支持工作、睡眠、待机、关机工作模    |
| 安全特性      | 硬件加密模块：RC4256、AES128、DES/3DES、SHA1/MD5、CRC32、2048 RSA、真随机数发生器 |
| 功耗 (典型值) | 持续发射：240mA@11b 1Mbps持续接收：95m                       |
| 供电范围      | 供电电压3.0V ~ 3.6V，供电电流 >300mA                         |
| 工作温度      | -20 ℃ ~ 85 ℃                                                 |
| 存储环境      | -40 ℃ ~ 90 ℃，< 90%RH                                        |



## [原理图](resource/Neptunes_schematic.pdf)

## Pin-Assignment

![1625027676056](assets/README.assets/1625027676056.png)

## 参考文档

1. [产品参数](#产品参数)
2. [Pin Assignment](#Pin-Assignment)
3. [软件使用手册（旧的1.0代码，不推荐）](软件使用手册.md)
4. [软件使用手册（最新master分支，推荐）](软件使用手册（最新master代码）.md)
5. [烧录工具使用手册](烧录工具使用手册.md)
6. [原理图](resource/Neptunes_schematic.pdf)
7. [烧录工具](resource/SecureCRSecureFXPortable.rar)
8. [代码样例](sample)



