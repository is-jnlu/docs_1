/*****************************************************************************
*
* File Name : wm_gpio_demo.c
*
* Description: gpio demo function
*
* Copyright (c) 2014 Winner Micro Electronic Design Co., Ltd.
* All rights reserved.
*
* Author : dave
*
* Date : 2014-6-2
*****************************************************************************/
#include "wm_include.h"

#include "ohos_init.h"
#include "cmsis_os2.h"

#include <unistd.h>

#define DEMO_ISR_IO		WM_IO_PB_09

#define DEMO_LED_IO		WM_IO_PB_08

static void demo_gpio_isr_callback(void *context)
{
	u16 ret;

	ret = tls_get_gpio_irq_status(DEMO_ISR_IO);
	printf("\nint flag =%d\n",ret);
	if(ret)
	{
		tls_clr_gpio_irq_status(DEMO_ISR_IO);
		ret = tls_gpio_read(DEMO_ISR_IO);
		printf("\nafter int io =%d\n",ret);
	}
}


int gpio_demo_task(void *arg)
{
    arg = arg;

    int ret;

    gpio_isr_test();

    tls_gpio_cfg(DEMO_LED_IO, WM_GPIO_DIR_OUTPUT, WM_GPIO_ATTR_PULLHIGH);

    while(1)
    {
        tls_gpio_write(DEMO_LED_IO,0);		
		ret = tls_gpio_read(DEMO_LED_IO);
		printf(" pullhigh value==[%d]\n", ret);

        sleep(1);

        tls_gpio_write(DEMO_LED_IO,1);	
		ret = tls_gpio_read(DEMO_LED_IO);
		printf(" pullhigh value==[%d]\n", ret);

        sleep(1);
    }
}


int gpio_isr_test(void)
{
	u16 gpio_pin;

	gpio_pin = DEMO_ISR_IO;


	tls_gpio_cfg(gpio_pin, WM_GPIO_DIR_INPUT, WM_GPIO_ATTR_PULLHIGH);
	tls_gpio_isr_register(gpio_pin, demo_gpio_isr_callback, NULL);
	tls_gpio_irq_enable(gpio_pin, WM_GPIO_IRQ_TRIG_RISING_EDGE);
	printf("\ntest gpio %d rising isr\n",gpio_pin);
	return WM_SUCCESS;
}


void W800_gpio_demo(void)
{
    osThreadAttr_t attr;
    
    attr.name = "gpio_demo_task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 2048;
    attr.priority = 26;

    if (osThreadNew((osThreadFunc_t)gpio_demo_task, NULL, &attr) == NULL) {
        printf("[key_demo] Falied to create KeyTask!\n");
    }

}
