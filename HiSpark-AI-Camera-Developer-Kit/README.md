# 润和**AI-Camera**开发套件资料

## 海思Linux内核相关资料

https://gitee.com/openharmony/device_soc_hisilicon/tree/master/hi3516dv300/sdk_linux/sample/taurus

### docker环境：

https://gitee.com/hihope_iot/embedded-race-hisilicon-track-2022/blob/master/OpenHarmony%E6%95%99%E7%A8%8B%E6%96%87%E6%A1%A3/OpenHarmony%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E2%80%94docker.md

#### （1）基础入门：

[HI3516环境搭建](doc/HI3516环境搭建.md)

[AI Camera烧录指导](doc/AICamera烧录指导.md)

[编写helloworld程序（南向）](doc/编写helloworld程序（南向）.md)

[DevEco Studio安装.md](doc/DevEco Studio安装.md)

[GPIO RGB LED灯操作](https://gitee.com/liangkzgitee/led_rgb)

#### （2）小型系统入门：

[小型系统应用开发入门HAP的编译打包](doc/小型系统应用开发入门HAP的编译打包.md)

[安装HAP包](doc/安装HAP包.md)

[知识体系中控面板案例—基于OpenHarmony最新代码](https://gitee.com/hihope_iot/docs/blob/master/HiSpark-AI-Camera-Developer-Kit/demo/demo_cenctrl/%E7%9F%A5%E8%AF%86%E4%BD%93%E7%B3%BB%E4%B8%AD%E6%8E%A7%E9%9D%A2%E6%9D%BF%E6%A1%88%E4%BE%8B%E2%80%94%E5%9F%BA%E4%BA%8EOpenHarmony%E6%9C%80%E6%96%B0%E4%BB%A3%E7%A0%81.md)

## 原理图

[原理图pdf](原理图)

## 安装说明书

[安装说明书pdf](InstallationGuide/AI_Camera_安装说明书.pdf)